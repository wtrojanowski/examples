<?php

namespace EG\AdminBundle\Repository\Production;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use EG\AdminBundle\Entity\Production\Device;

/**
 * Class DeviceRepository
 */
class DeviceRepository extends EntityRepository
{
    /**
     * Get single socket entity for ID
     *
     * @return Socket|null
     */
    public function getOne(int $id): ?Device
    {
        return $this->_em
            ->createQueryBuilder()
            ->select(['d'])
            ->from(Device::class, 'd')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * Receive query for getting all of production devices
     *
     * @return Query
     */
    public function getAllQuery(): Query
    {
        return $this->_em
            ->createQueryBuilder()
            ->select(['d'])
            ->from(Device::class, 'd')
            ->innerJoin('d.socket', 's')
            ->getQuery()
        ;
    }
}