<?php

namespace EG\ProductFlowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\{
    Collection, ArrayCollection
};
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use EG\ProductFlowBundle\Entity\Interfaces\EntityCreatedBy;
use EG\ProductFlowBundle\Entity\Interfaces\UploadTarget;
use Symfony\Component\Validator\Constraints as Assert;

use EG\ApiBundle\Entity\SFDC\Account;
use EG\AdminBundle\Entity\FileCategory;
use EG\AdminBundle\Entity\Dictionaries\Product\{
    Material, Status, Source, PrintType, ToperType
};
use EG\AdminBundle\Entity\Dictionaries\Color\{
    SidesExternal, SidesInside, BackExternal, BackInside, ShelfFront, ShelfInterior, ColorFooter, ColorToper
};

use EG\EstimateBundle\Entity\{
    Estimate, EstimateGroup
};

use EG\ProductFlowBundle\Traits;
use Doctrine\Common\Collections\Criteria;

use EG\AdminBundle\Entity\Dictionaries\Estimate\Status as EstimateStatus;
use EG\ConstructionBundle\Entity\Construction;

/**
 * @ORM\Entity(repositoryClass="EG\ProductFlowBundle\Repository\VersionRepository")
 * @ORM\Table(name="product_versions")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class Version implements UploadTarget, EntityCreatedBy
{
    use TimestampableEntity;
    use Traits\EntityCreatedBy;
    
    /**
     * @var int
     * @ORM\Id
     * @Serializer\Expose()
     * @Serializer\Type("int")
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     * @Assert\Length(min="3", max="90")
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=90, nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     * @Assert\Type(type="string")
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $description;

    /**
     * @var File
     * @ORM\OneToOne(targetEntity="File", inversedBy="version")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $thumbnail;

    /**
     * @var Status
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="EG\AdminBundle\Entity\Dictionaries\Product\Status")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    protected $status;
     
    /**
     * @var PrintType
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="EG\AdminBundle\Entity\Dictionaries\Product\PrintType")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    protected $printType;

    /**
     * @var Collection|File[]
     * @ORM\ManyToMany(targetEntity="File")
     * @ORM\JoinTable(name="product_version_files")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $files;

    /**
     * @var Collection|Estimate[]
     * @ORM\OneToMany(targetEntity="EG\EstimateBundle\Entity\Estimate", mappedBy="version", cascade={"persist", "remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $estimates;

    /**
     * @var Collection|EstimateGroup[]
     * @ORM\OneToMany(targetEntity="EG\EstimateBundle\Entity\EstimateGroup", mappedBy="version", cascade={"persist", "remove"})
     */
    protected $estimateGroups;

    /**
     * Version constructor.
     */
    public function __construct()
    {
        $this->estimates = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * Get the value of id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  integer $id
     * @return self
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the value of description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string $description
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get the value of thumbnail
     *
     * @return File
     */
    public function getThumbnail(): ?File
    {
        return $this->thumbnail;
    }

    /**
     * Set the value of thumbnail
     *
     * @param  File $file
     * @return self
     */
    public function setThumbnail(File $file): self
    {
        $this->thumbnail = $file;
        return $this;
    }

    /**
     * Get the value of status
     *
     * @return  Status
     */
    public function getStatus(): ?Status
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @param  Status $status
     * @return self
     */
    public function setStatus(?Status $status): self
    {
        $this->status = $status;
        return $this;
    }
    
    /**
     * Get the value of print type
     *
     * @return PrintType|null
     */
    public function getPrintType(): ?PrintType
    {
        return $this->printType;
    }

    /**
     * Set the value of print type
     *
     * @param  PrintType|null $printType
     * @return self
     */
    public function setPrintType(?PrintType $printType): self
    {
        $this->printType = $printType;
        return $this;
    }

    /**
     * Get the collection of files
     *
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * Set the collection of files
     *
     * @param  Collection|File[] $files
     * @return self
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
        return $this;
    }

    /**
     * Add item to files collection
     *
     * @param  File $file
     * @return self
     */
    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }
        return $this;
    }

    /**
     * Remove item from files collection
     *
     * @param  File $file
     * @return self
     */
    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }
        return $this;
    }


    /**
     * Produces path for uploaded file, eg: "P000001_V002
     *
     * @param File $file
     * @return string
     */
    public function getPath(File $file): string
    {
        $product = str_pad($this->getProduct()->getId(), 6, '0', STR_PAD_LEFT);
        $version = $this->getCode();
        $category = mb_strtoupper($file->getCategory()->getShortName());
        return "P{$product}_V{$version}/$category";
    }

    /**
     * Get the latest Estimate
     *
     * @return Estimate
     */
    public function getCurrentEstimate(?int $amount = null): ?Estimate
    {
        $status = null;
        foreach ($this->estimates as $estimate) {
            $estimateStatus = $estimate->getStatus();
            if ($estimateStatus->getCodeName() == 'STATUS_CURRENT') {
                $status = $estimateStatus;
                break;
            }
        }

        $printType = $this->printType->getCodeName();
        $criteria = Criteria::create();
        
        switch ($printType) {
            case 'PRINT_TYPE_A':
                $criteria = Criteria::create();
                $criteria = $criteria->where(Criteria::expr()->eq('status', $status));
                if (!empty($amount)) {
                    $div = intval(str_pad(1, strlen($amount), 0));
                    if ($div == 1) {
                        $div = 10;
                    }
                    $minAmount = floor($amount / $div) * $div;
                    $maxAmount = ceil($amount / $div) * $div;
                    if ($minAmount == $maxAmount) {
                        $maxAmount = ceil(($amount + 1) / $div) * $div;
                    }
                    $criteria = $criteria->andWhere(Criteria::expr()->eq('isVisible', true));
                    $criteria = $criteria->andWhere(Criteria::expr()->gte('amount', $minAmount));
                    $criteria = $criteria->andWhere(Criteria::expr()->lte('amount', $maxAmount));
                }
                $criteria = $criteria->orderBy(array('price' => Criteria::DESC))->setMaxResults(1);
                break;
            case 'PRINT_TYPE_B':
            default:
                $criteria = $criteria->where(Criteria::expr()->eq('status', $status));
                $criteria = $criteria->andWhere(Criteria::expr()->eq('isVisible', true));
                $criteria = $criteria->orderBy(array('createdAt' => Criteria::DESC))->orderBy(array( 'id' => Criteria::DESC))->setMaxResults(1);
                break;
        }
        $estimate = $this->estimates->matching($criteria)->current();

        return $estimate === false ? null : $estimate;
    }

    /**
     * Get the value of estimates
     *
     * @return Collection|Estimate[]
     */
    public function getEstimates(?EstimateStatus $status = null, array $sortCriteria = array(['status', 'ASC'])): Collection
    {
        $criteria = Criteria::create();
        if (isset($status)) {
            $criteria->where(Criteria::expr()->eq('status', $status));
        }
        foreach ($sortCriteria as $sort) {
            if (is_array($sort)) {
                $criteria = $criteria->orderBy(array($sort[0] => $sort[1]));
            }
        }

        return $this->estimates->matching($criteria);
    }

    /**
     * Set the value of estimates
     *
     * @param  Collection|Estimate[] $estimates
     * @return self
     */
    public function setEstimates(Collection $estimates)
    {
        $this->estimates = $estimates;
        return $this;
    }

    /**
     * Add item to estimates collection
     *
     * @param  Estimate $estimate
     * @return self
     */
    public function addEstimate(Estimate $estimate): self
    {
        if (!$this->estimates->contains($estimate)) {
            $estimate->setVersion($this);
            $this->estimates->add($estimate);
        }
        return $this;
    }

    /**
     * Remove item from estimates collection
     *
     * @param  Estimate $estimate
     * @return self
     */
    public function removeEstimate(Estimate $estimate): self
    {
        if ($this->estimates->contains($estimate)) {
            $this->estimates->removeElement($estimate);
        }
        return $this;
    }

    /**
     * Get the value of estimateGroups
     *
     * @return Collection|EstimateGroup[]
     */
    public function getEstimateGroups(): Collection
    {
        return $this->estimateGroups;
    }

    /**
     * Set the value of estimateGroups
     *
     * @param  Collection|EstimateGroup[] $estimateGroups
     * @return self
     */
    public function setEstimateGroups(Collection $estimateGroups)
    {
        $this->estimateGroups = $estimateGroups;
        return $this;
    }

    /**
     * Add item to estimateGroups collection
     *
     * @param  EstimateGroup $estimateGroup
     * @return self
     */
    public function addEstimateGroup(EstimateGroup $estimateGroup): self
    {
        if (!$this->estimateGroups->contains($estimateGroup)) {
            $estimateGroup->setVersion($this);
            $this->estimateGroups->add($estimateGroup);
        }
        return $this;
    }

    /**
     * Remove item from estimateGroups collection
     *
     * @param  EstimateGroup $estimateGroup
     * @return self
     */
    public function removeEstimateGroup(EstimateGroup $estimateGroup): self
    {
        if ($this->estimateGroups->contains($estimateGroup)) {
            $this->estimateGroups->removeElement($estimateGroup);
        }
        return $this;
    }
}
