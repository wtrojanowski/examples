<?php

namespace AppBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Abstract info
 */
abstract class AbstractInfo
{
	/**
	 * Entity Manger
	 *
	 * @var ObjectManager
	 */
	protected $entityManager;

	/**
	 * Token Storage
	 *
	 * @var TokenStorageInterface
	 */
	protected $tokenStorage;
	
	/**
	 * Constructor 
	 * @param ObjectManager $entityManager
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function __construct(ObjectManager $entityManager, TokenStorageInterface $tokenStorage)
	{
		$this->entityManager = $entityManager;
		$this->tokenStorage = $tokenStorage;
	}
}

/**
 * Author Zone info
 */
class AuthorZoneInfo extends AbstractInfo
{
	/** 
	 * Gets logged user articles count
	 *
	 * @returns int
	 */
	public function getLoggedUserArticlesCount(): int
	{
		// Gets current user
		$currentUser = $this->tokenStorage->getToken()->getUser();

		// Gets repository of Article entity
		$repoArticle = $this->entityManager->getRepository("AppBundle:Article");

		// Gets count of articles
		$queryBuilder = $repoArticle->createQueryBuilder('a');
		$queryBuilder->select('count(a.id)');
		$queryBuilder->where("a.user = :user");
		$queryBuilder->setParameter('user', $currentUser);

		return $queryBuilder->getQuery()->getSingleScalarResult();
	}
}

/**
 * Admin Zone info
 */
class AdminZoneInfo extends AbstractInfo
{
	/**
	 * Gets all users count
	 *
	 * @returns int
	 */
	public function getUsersCount(): int
	{
		// Gets repository of User entity
		$repoUser = $this->entityManager->getRepository('AppBundle:User');
		
		// Gets count of users
		$queryBuilder = $repoUser->createQueryBuilder('u');
		$queryBuilder->select('count(u.id)');
		
		return $queryBuilder->getQuery()->getSingleScalarResult();
	}
	
	/**
	 * Gets all articles count
	 *
	 * @returns int
	 */
	public function getArticlesCount(): int
	{
		// Gets repository of Article entity
		$repoArticle = $this->em->getRepository('AppBundle:Article');
		
		// Gets count of articles
		$queryBuilder = $repoArticle->createQueryBuilder('a');
		$queryBuilder->select('count(a.id)');
		
		return $queryBuilder->getQuery()->getSingleScalarResult();
	}
}

/**
 * Info Service
 */
class InfoService
{
	private $authorInfo;
	private $adminInfo;
	
	/**
	 * Constructor 
	 * @param DoctrineRegistry $doctrine
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function __construct(DoctrineRegistry $doctrine, TokenStorageInterface $tokenStorage)
	{
		parent::__construct($doctrine->getManager(), $tokenStorage);
	}
	
	/**
	 * Gets info based on Class and Method Name
	 * @param string $className
	 * @param string $methodName
	 */
	public function get(string $className, string $methodName): int
	{
		$className =  __NAMESPACE__ . '\\' . ucfirst(strtolower($className)) . 'Info';
		if (class_exists($className)) {
			$methodName = 'get' . $methodName;
			if (method_exists($className, $methodName)) {
				$objectName = strtolower($className) . 'Info';
				$this->$objectName = new $className($this->entityManager, $this->tokenStorage);
				return $this->$objectName->$methodName();
			} else {
				throw new \Exception('Method ' . $methodName . ' does not exist');
			}
		} else {
			throw new \Exception('Class ' . $className . ' does not exist');
		}
	}
}