<?php
namespace AppBundle\Service;

/**
 * Pagination Service
 */
class Pagination
{	
	const ROWS_PER_PAGE = 10;
	const LINKS_VISIBLE = 3;
	
	private $info;
	private $twig;
	
	public function __construct($info = null, $twig = null)
	{
		$this->info = $info;
		$this->twig = $twig;
	}
	
	public function getLimit()
	{
		return self::ROWS_PER_PAGE;
	}
	
	public function getOffset($page)
	{
		return ($page - 1) * self::ROWS_PER_PAGE;
	}
	
	public function generate($className, $methodName, $page)
	{		
		$pages = array();
		
		$count = $this->info->get($className, $methodName);
			
		$pagesCount = ceil($count / self::ROWS_PER_PAGE);
		$linksVisible = self::LINKS_VISIBLE;
		
		$startPage = ceil($page / $linksVisible) * $linksVisible - $linksVisible + 1;
		$endPage = $startPage + $linksVisible -1;
		
		if ($endPage > $pagesCount) {
			$endPage = $pagesCount;
		}
		
		for ($i = $startPage; $i <= $endPage; $i++) {
			$pages[] = $i;
		}
		
		$pager = array(
			'first_page' => 1,
			'last_page' => $pagesCount,
			'links_visible' => $linksVisible,
			'pages' => $pages
		);

		return $this->twig->render('partial/pagination.html.twig', array('current_page' => $page, 'pages' => $pager));	
	}	
}