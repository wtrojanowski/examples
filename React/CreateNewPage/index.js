import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { isEmpty, upperFirst } from 'lodash';
import { FocusContainer, TextField, Button, CardActions, DialogContainer } from 'react-md';
import { toast } from 'react-toastify';
import Loader from '../SharedComponents/Loader';
import Header from '../SharedComponents/Header';
import Notification from '../SharedComponents/Notification';
import { addItem } from '../store/items/actions';
import { storeItem } from '../store/item/actions';
import { getFirebaseSnapshot, updateItemStatus } from '../utils/firebase';
import { createItemChannel } from '../utils/mam';
import { STATUS_ACTIVE, STATUS_LOST, STATUS_DELETED, STATUS_FOUND } from '../utils/statusList.js';
import '../assets/scss/createItemPage.scss';
import { BrowserQRCodeReader } from '@zxing/library'

const codeReader = new BrowserQRCodeReader();

class CreateItemPage extends Component {
  state = {
    showLoader: false,
    idError: false,
    destinationError: false,
    departureError: false,
    cargoError: false,
    typeError: false,
    id: ''
  };

  componentDidMount() {
    const { user, history } = this.props;
    if (isEmpty(user)) {
      history.push('/login');
    }
  }

  notifySuccess = message => toast.success(message);
  notifyError = message => toast.error(message);

  validate = () => {
    this.setState({
      idError: !this.state.id,
    });

    return !this.state.id;
  };

  startScanner = async () => {
    const devices = await codeReader.getVideoInputDevices()
    if(devices.length) {
      const firstDeviceId = devices[0].deviceId;

      codeReader.decodeFromInputVideoDevice(firstDeviceId, 'video-area')
      .then(result => {
        this.setState({ id: result.text })
      })
      .catch(err => console.error(err));
    } else {
      this.notifyError('Please check your video inputs!, we cant find any');
    }

  }

  stopScanner = () => {
    codeReader.reset()
  }
  handleTextChange = textID => {
    this.setState({ id : textID })
  }
  onError = error => {
    this.setState({ showLoader: false });
    this.notifyError(error || 'Something went wrong');
  };

  createItem = async event => {
    event.preventDefault();
    const formError = this.validate();
    const { history, storeItem, addItem, user, project } = this.props;

    if (!formError) {
      const { id, previousEvent } = user;
      const request = {
        owner: id,
        status: previousEvent[0]
      };
      // Format the item ID to remove dashes and parens
      const itemId = this.state.id.replace(/[^0-9a-zA-Z_-]/g, '');
      const firebaseSnapshot = await getFirebaseSnapshot(itemId, this.onError);
      if (firebaseSnapshot === null) {
        this.setState({ showLoader: true });
        const eventBody = await createItemChannel(project, itemId, request, id);

        await addItem(itemId);
        await storeItem([eventBody]);

        history.push(`/details/${itemId}`);
      } else {
        if (firebaseSnapshot.cStatus == STATUS_DELETED) {
          this.showDialog();
        } else {
          this.notifyError(`${upperFirst(project.trackingUnit)} exists`);
        }
      }
    } else {
      this.notifyError('Error with some of the input fields');
    }
  };

  //Zapisuje status po stronie bazy Firebase
  changeStatus = async (newStatus) => {
    const itemId = this.state.id.replace(/[^0-9a-zA-Z_-]/g, '');
    var res = await updateItemStatus(itemId, newStatus, this.onError);
    this.setState({dialogVisible: false});
    this.props.history.push(`/details/${itemId}`);
  }

  showDialog = () => {
    this.setState({dialogVisible: true});
  }

  hideDialog = () => {
    this.setState({dialogVisible: false});
  }

  render() {
    const { showLoader, idError, dialogVisible } = this.state;
    const {
      history,
      project: { trackingUnit },
    } = this.props;

    const unit = upperFirst(trackingUnit);

    const dialogActions = [{
      onClick: () => this.changeStatus(STATUS_ACTIVE),
      primary: true,
      children: 'Yes',
    }, {
      onClick: this.hideDialog,
      primary: true,
      children: 'No',
    }];

    return (
      <div>
        <Header>
          <div>
            <div>
              <a onClick={() => history.push('/')}>
                <img src="arrow_left.svg" alt="back" />
              </a>
              <span>Create new {trackingUnit}</span>
            </div>
          </div>
        </Header>
        <div className="create-item-wrapper">
          <FocusContainer
            focusOnMount
            containFocus
            component="form"
            className="md-grid"
            onSubmit={this.createItem}
            aria-labelledby="create-item"
          >
            <div className="input-wrapper">
              <TextField
                value={this.state.id}
                onChange={this.handleTextChange}
                id="itemId"
                label={`${unit} ID`}
                required
                type="text"
                error={idError}
                errorText="This field is required."
              />

              <Button onClick={this.startScanner} raised primary swapTheming>Start</Button>
              <Button onClick={this.stopScanner} raised secondary iconChildren="close" swapTheming>Stop</Button>
            </div>
            <video id="video-area"></video>

          </FocusContainer>
          <Notification />
          <DialogContainer
            id="add-dialog"
            visible={dialogVisible}
            title={`The rack exists but it is ${STATUS_DELETED}, would you like to ${STATUS_ACTIVE} it?`}
            onHide={this.hideDialog}
            aria-describedby="add-dialog"
            modal
            actions={dialogActions}
          >
          </DialogContainer>
          <div>
            <Loader showLoader={showLoader} />
            <CardActions className={`md-cell md-cell--12 ${showLoader ? 'hidden' : ''}`}>
              <Button className="iota-theme-button" raised onClick={this.createItem}>
                Create (add)
              </Button>
            </CardActions>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  project: state.project,
});

const mapDispatchToProps = dispatch => ({
  addItem: itemId => dispatch(addItem(itemId)),
  storeItem: item => dispatch(storeItem(item)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateItemPage));
