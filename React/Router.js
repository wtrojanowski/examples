import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import configureStore from './store/configure';
import ListPage from './ListPage';
import DetailsPage from './DetailsPage';
import BaseDetailsPage from './DetailsPage/baseDetails.js';
import InformationPage from './InformationPage';
import LoginPage from './LoginPage';
import AnonymousPage from './AnonymousPage';
import CreateNewPage from './CreateNewPage';

const store = configureStore();

const Router = () => (
  <Provider store={store}>
    <Switch>
      <Route path="/" component={ListPage} exact />
      <Route path="/information" component={InformationPage} />
      <Route path="/details/:itemId" component={DetailsPage} />
      <Route path="/basedetails/:itemId" component={BaseDetailsPage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/anonymous" component={AnonymousPage} />
      <Route path="/new" component={CreateNewPage} />
      <Route component={ListPage} />
    </Switch>
  </Provider>
);

export default Router;
